// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "HiveAttack.h"
#include "HiveStatics.generated.h"

class UTextureRenderTarget2D;

/**
 * 
 */
UCLASS()
class HIVE_API UHiveStatics : public UBlueprintFunctionLibrary {
	GENERATED_BODY()
	
public:

};

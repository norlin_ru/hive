// Fill out your copyright notice in the Description page of Project Settings.

#include "HivePlayerController.h"
#include "Engine/GameViewportClient.h"

void AHivePlayerController::BeginPlay() {
	Super::BeginPlay();

	FViewport::ViewportResizedEvent.AddUObject(this, &AHivePlayerController::OnViewportResized);

	OnViewportResized(GEngine->GameViewport->Viewport, 0);
}

void AHivePlayerController::OnViewportResized(FViewport* Viewport, uint32 Unused) {
	if (!Viewport || Viewport != GEngine->GameViewport->Viewport) {
		return;
	}

	ULocalPlayer* LocalPlayer = GetLocalPlayer();

	const FIntPoint ViewportSize = Viewport->GetSizeXY();
	if (ViewportSize.X > ViewportSize.Y) {
		LocalPlayer->AspectRatioAxisConstraint = AspectRatio_MaintainYFOV;
	} else {
		LocalPlayer->AspectRatioAxisConstraint = AspectRatio_MaintainXFOV;
	}
}

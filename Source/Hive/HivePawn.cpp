// Fill out your copyright notice in the Description page of Project Settings.

#include "HivePawn.h"
#include "Utils.h"
#include "CollisionChannels.h"

#include "Containers/Set.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"

// Sets default values
AHivePawn::AHivePawn() {
	PrimaryActorTick.bCanEverTick = true;
}

void AHivePawn::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	ProcessAttacks();
}

void AHivePawn::LaunchAttack(const FHiveAttack& Attack) {
	const int32 AttackIndex = PendingAttacks.Add(Attack);

	OnAttackLaunch(PendingAttacks[AttackIndex]);
}

void AHivePawn::ProcessAttacks() {
	auto AttackIterator = PendingAttacks.CreateIterator();

	while (AttackIterator) {
		FHiveAttack& Attack = *AttackIterator;

		if (TraceAttack(Attack)) {
			AttackIterator.RemoveCurrent();
		}

		AttackIterator++;
	}
}

bool AHivePawn::TraceAttack(FHiveAttack& Attack) {
	UWorld* World = GetWorld();

	const ECollisionChannel AttackTraceChannel = ECC_Hive_Interactive;
	const FCollisionQueryParams Params;

	const float Angle = Attack.Angle;
	const FVector Origin = Attack.Origin;

	const float LastDistance = Attack.LastDistance;

	const float TimeElapsed = World->GetTimeSeconds() - Attack.LaunchTime;
	const float DistanceTravelled = Attack.InitialDistance + TimeElapsed * Attack.Speed;

	const bool AttackRangeExceeded = DistanceTravelled >= Attack.Range;

	const float AngularStep = Angle / AttackResolution;
	const FVector InitialVector = Attack.Direction.RotateAngleAxis(-Angle / 2, FVector::UpVector);

	// Count trace hits by actor
	TMap<AActor*, int32> HitsCounter;

	for (int32 Index = 0; Index < AttackResolution; Index += 1) {
		if (Attack.HitPixels.Contains(Index)) {
			continue;
		}

		const FVector TraceVector = InitialVector.RotateAngleAxis(AngularStep * Index, FVector::UpVector);

		// Trace from previous frame position to the travelled distance
		const FVector PreviousTracePosition = Origin + TraceVector * LastDistance;
		const FVector CurrentTracePosition = Origin + TraceVector * DistanceTravelled;

		FHitResult Hit;
		if (!World->LineTraceSingleByChannel(Hit, PreviousTracePosition, CurrentTracePosition, AttackTraceChannel, Params)) {
			//DrawDebugLine(World, PreviousTracePosition, CurrentTracePosition, FColor::Red, false, 0.1f, 1, 2.f);
			continue;
		}

		Attack.HitPixels.Add(Index);

		//DrawDebugLine(World, PreviousTracePosition, CurrentTracePosition, FColor::Blue, false, 0.1f, 2, 2.f);

		AActor* Actor = Hit.GetActor();

		int32* HitsCount = HitsCounter.Find(Actor);
		if (HitsCount) {
			*HitsCount += 1;
		} else {
			HitsCounter.Emplace(Actor, 1);
		}
	}

	// Update last distance
	Attack.LastDistance = DistanceTravelled;

	if (HitsCounter.Num() == 0) {
		// NGLOG("No one hit!");
		return AttackRangeExceeded;
	}

	for (const TPair<AActor*, int32> Hit : HitsCounter) {
		AActor* Target = Hit.Key;
		const float Damage = (float)Hit.Value / (float)AttackResolution;
		// NGLOG("Hit: %s, damage: %f", *Target->GetName(), Damage);

		TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
		FDamageEvent DamageEvent(ValidDamageTypeClass);

		Target->TakeDamage(Damage * Attack.Power, DamageEvent, GetController(), this);
	}

	return AttackRangeExceeded;
}

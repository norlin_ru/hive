// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "HiveAttack.h"
#include "HivePawn.generated.h"

UCLASS()
class HIVE_API AHivePawn : public APawn {
	GENERATED_BODY()

public: // APawn

	AHivePawn();

	void Tick(float DeltaTime) override;

protected: // Methods

	UFUNCTION(BlueprintCallable, Category = "Hive")
	void LaunchAttack(const FHiveAttack& Attack);

	UFUNCTION(BlueprintImplementableEvent, Category = "Hive")
	void OnAttackLaunch(const FHiveAttack& Attack);
	
	void ProcessAttacks();

	// Returns true if attack range is exceeded
	bool TraceAttack(FHiveAttack& Attack);

protected: // Settings

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
	int32 AttackResolution = 256;

protected: // Variables

	TArray<FHiveAttack> PendingAttacks;

};

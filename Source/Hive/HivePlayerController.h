// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "HivePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class HIVE_API AHivePlayerController : public APlayerController {
	GENERATED_BODY()
	
public: // APlayerController

	void BeginPlay() override;

protected: // Internal methods

	void OnViewportResized(FViewport* Viewport, uint32 Unused);

};

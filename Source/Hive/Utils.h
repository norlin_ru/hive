// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define NGLOG(Format, ...) \
  UE_LOG(LogTemp, Warning, TEXT("%s:%d: " Format), TEXT(__FUNCTION__), __LINE__, ##__VA_ARGS__)

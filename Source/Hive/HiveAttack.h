// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HiveAttack.generated.h"

USTRUCT(BlueprintType)
struct FHiveAttack {
    GENERATED_BODY()

public: // Fields

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    int32 id = INDEX_NONE;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    FVector Origin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    float LaunchTime = 0.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    FVector Direction;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    float Angle = 0.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    float Speed = 0.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    float InitialDistance = 0.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    float Range = 0.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    float Power = 0.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    float LastDistance = 0.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hive")
    TArray<int32> HitPixels;

};
